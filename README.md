# ProForma Customer API #

ProForma provides an API to make it easier for our customers to access their data. This repository contains documentation
in OpenAPI 3.0 format. 

## Version policy

The ProForma Customer API uses [semantic versioning](https://semver.org/). This means that we strive to maintain backwards compatibility,
and new features will be added to the API without breaking existing functionality. All changes to the API will require a new version.


* Bug fixes are treated a 'patch' version. This is indicated by the third digit of the version number, eg 1.0.0 → 1.0.1

* New features which do not change existing functionality are treated as a 'minor' version. This is indicated by the second digit of the
version number, eg 1.0.0 → 1.1.0

* New features which _do_ change existing functionality are treated as a 'major' version. This is indicated by the first digit of the
version number, eg 1.0.0 → 2.0.0. If we release a new major version it is our intention that the previous version will still be available
and be maintained until customers can migrate to the new one.
